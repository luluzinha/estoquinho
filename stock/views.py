from django.shortcuts import render
from django.db.models import Q
from django.views.generic import ListView, UpdateView, DetailView
from django.views.generic.edit import CreateView
from stock.models import Product

from django.shortcuts import redirect
from stock.models import Product, Category, Comment

class LayoutView:
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context

class ProductListView(ListView, LayoutView):
    model = Product

class CategoryDetailView(LayoutView, DetailView):
    model = Category

class ProductDetailView(LayoutView, DetailView):
    model = Product

class CommentDetailView(LayoutView, DetailView):
    model = Comment

class CommentCreateView(LayoutView, CreateView):
    model = Comment
    fields = ['text']

    def form_valid(self, form):
        post_id = self.kwargs['post']
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.product = Product.objects.get(id=product_id)
        self.object.save()
        return redirect('product-detail', pk=product_id)

class OutOfStockListView(ListView):
    model = Product

    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(quantity=0)

class AddStockView(UpdateView):
    model = Product
    
    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantity = self.request.POST['quantity']
        product.quantity += int(quantity)
        product.save()
        return redirect('product-list')

class RemoveStockView(UpdateView):
    model = Product
    
    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantity = self.request.POST['quantity']
        product.quantity -= int(quantity)
        product.save()
        return redirect('product-list')
