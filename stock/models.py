from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length = 20)

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length = 45)
    description = RichTextField()
    code = models.CharField(max_length = 12)
    image = models.ImageField(null=True, upload_to='products')
    quantity = models.PositiveIntegerField()
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True, null=True, related_name='products')  

    def __str__(self):
        return self.name

class Comment(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add = True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=True, null=True, related_name='comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')