"""projeto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from stock import views as stock_views

urlpatterns = [
    path('', stock_views.ProductListView.as_view()),
    path('', stock_views.ProductListView.as_view(), name='product-list'),
    path('comentario/<int:product>/', stock_views.CommentCreateView.as_view(), name='comment'),
    path('categoria/<int:pk>/', stock_views.CategoryDetailView.as_view(), name='category-detail'),
    path('product/<int:pk>/', stock_views.ProductDetailView.as_view(), name='product-detail'),
    path('fora-de-estoque/', stock_views.OutOfStockListView.as_view(), name='out-of-stock'),
    path('adicionar-estoque/<int:pk>/', stock_views.AddStockView.as_view(), name='add-stock'),
    path('remover-estoque/<int:pk>/', stock_views.RemoveStockView.as_view(), name='remove-stock'),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)